<?php

namespace app\controllers;

use app\models\Category;
use app\models\Pages;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PostsController implements the CRUD actions for Posts model.
 */
class CategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['slug'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $slug - category
     * @return string - render page
     * @throws NotFoundHttpException
     */
    public function actionSlug($slug)
    {
        $category = Category::findOne(['slug' => $slug, 'type' => true]);
        if ($category) {
            $provider = new ActiveDataProvider([
                'query' => Pages::find()->where(['category_id' => $category->id]),
                'pagination' => [
                    'pageSize' => 1,
                ],
            ]);
            return $this->render('list', ['provider' => $provider, 'category' => $category]);
        }
        throw new NotFoundHttpException();
    }
}