<?php

namespace app\controllers;

use app\models\Category;
use app\models\Pages;
use Codeception\PHPUnit\Constraint\Page;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * PostsController implements the CRUD actions for Posts model.
 */
class PageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['slug'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $slug - category
     * @param $subSlug - pages
     * @return string - render page
     * @throws BadRequestHttpException
     */
    public function actionSlug($slug, $subSlug)
    {
        $page = Pages::find()
            ->joinWith(['category'], false)
            ->where(['category.slug' => $slug, 'pages.slug' => $subSlug])
            ->one();
        if ($page) {
            /**
             * @var $page Pages
             */
            Yii::$app->view->registerMetaTag([
                'name' => 'title',
                'content' => $page->meta_title
            ]);
            Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $page->meta_keywords
            ]);
            Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $page->meta_description
            ]);
            return $this->render('view', ['page' => $page]);
        }
        throw new BadRequestHttpException();
    }
}