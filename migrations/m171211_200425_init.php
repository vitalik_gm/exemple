<?php

use yii\db\Migration;

/**
 * Class m171211_200425_init
 */
class m171211_200425_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
            'slug' => $this->string(),
            'meta_title' => $this->string(),
            'meta_keywords' => $this->string(),
            'meta_description' => $this->text(),
            'type' => $this->boolean()->comment('0 - закритий лінк, 1 - відкритий лінк'),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);

        $this->createTable('{{%pages}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'text' => $this->text(),
            'slug' => $this->string(),
            'meta_title' => $this->string(),
            'meta_keywords' => $this->string(),
            'meta_description' => $this->text(),
            'category_id' => $this->integer(),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);
        $this->addForeignKey('fk-pages-category', '{{%pages}}', 'category_id', '{{%category}}', 'id', 'CASCADE');

        $category_array = [
            [
                'title' => 'Новини',
                'description' => 'Новини',
                'slug' => 'news',
                'meta_title' => 'Новини',
                'meta_keywords' => 'Новини',
                'meta_description' => 'Новини',
                'type' => true,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'title' => 'Статті',
                'description' => 'Статті',
                'slug' => 'articles',
                'meta_title' => 'Статті',
                'meta_keywords' => 'Статті',
                'meta_description' => 'Статті',
                'type' => true,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'title' => 'Форуми',
                'description' => 'Форуми',
                'slug' => 'forums',
                'meta_title' => 'Форуми',
                'meta_keywords' => 'Форуми',
                'meta_description' => 'Форуми',
                'type' => false,
                'created_at' => time(),
                'updated_at' => time()
            ]
        ];
        $this->batchInsert('{{%category}}', [
            'title', 'description', 'slug', 'meta_title', 'meta_keywords', 'meta_description',
            'type', 'created_at', 'updated_at'], $category_array);

        $pages_array = [
            [
                'title' => 'Перша новина',
                'text' => 'Текст новини',
                'slug' => 'first-news',
                'meta_title' => 'Перша новина',
                'meta_keywords' => 'Перша новина, новини',
                'meta_description' => 'Перша новина, новини',
                'category_id' => 1,
                'created_at' => time(),
                'updated_at' => time(),
            ],
            [
                'title' => 'Друга новина',
                'text' => 'Текст новини',
                'slug' => 'second-news',
                'meta_title' => 'Друга новина',
                'meta_keywords' => 'Друга новина, новини',
                'meta_description' => 'Друга новина, новини',
                'category_id' => 1,
                'created_at' => time(),
                'updated_at' => time(),
            ],
            [
                'title' => 'Перша стаття',
                'text' => 'Текст статті',
                'slug' => 'first-article',
                'meta_title' => 'Перша стаття',
                'meta_keywords' => 'Перша стаття, статті',
                'meta_description' => 'Перша статті, статті',
                'category_id' => 2,
                'created_at' => time(),
                'updated_at' => time(),
            ],
            [
                'title' => 'Друга стаття',
                'text' => 'Текст новини',
                'slug' => 'second-article',
                'meta_title' => 'Друга стаття',
                'meta_keywords' => 'Друга стаття, статті',
                'meta_description' => 'Друга стаття, статті',
                'category_id' => 2,
                'created_at' => time(),
                'updated_at' => time(),
            ],
            [
                'title' => 'Форум',
                'text' => 'Короткий опис',
                'slug' => 'first-forum',
                'meta_title' => 'Форум',
                'meta_keywords' => 'Форум, Форуми',
                'meta_description' => 'Форум, Форуми',
                'category_id' => 3,
                'created_at' => time(),
                'updated_at' => time(),
            ],
        ];
        $this->batchInsert('{{%pages}}', [
            'title', 'text', 'slug', 'meta_title', 'meta_keywords', 'meta_description',
            'category_id', 'created_at', 'updated_at'
        ], $pages_array);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-pages-category', '{{%pages}}');
        $this->dropTable('{{%pages}}');
        $this->dropTable('{{%category}}');
    }
}
