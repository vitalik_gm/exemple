<?php

/* @var $this yii\web\View
 * @var $page \app\models\Pages
 */

use yii\helpers\Html;

$this->title = $page->title;
$this->params['breadcrumbs'][] = ['label' => $page->category->title, 'url' => ['/' . $page->category->slug]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $page->text ?>
    </p>

</div>
