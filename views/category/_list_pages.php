<?php
/* @var $this yii\web\View
 * @var $page \app\models\Pages
 */

use yii\helpers\Html;

?>

<div class="row">
    <div class="col-lg-12"><?= Html::a($page->title, [Yii::$app->request->get('slug') . '/' . $page->slug]) ?></div>
</div>
