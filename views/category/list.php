<?php

/* @var $this yii\web\View
 * @var $provider \yii\data\ActiveDataProvider
 * @var $category \app\models\Category
 */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = $category->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= ListView::widget([
            'dataProvider' => $provider,
            'summary' => '',
            'layout' => '{summary}{items}<div class="clearfix"></div><div class="widget-pager">{pager}</div>',
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_list_pages', ['page' => $model]);
            }
        ]); ?>
    </p>

</div>
